import { defineStore } from "pinia";
import { IMeme } from "@/interfaces/IMeme";

export const useMemesStore=defineStore('memes',{
  state:()=>{
      const titleApp:string='Awesome memes App';
      let memes:Array<IMeme> = [];
      return {titleApp,memes}
  },

  actions:{
      async getMemes(params:number=1){
        try {
          const rawResponse= await fetch('https://api.imgflip.com/get_memes');
          const response=await rawResponse.json();
          if(params<100){
            this.memes=(response.data.memes).slice(0,params)
          }else{
            this.memes=response.data.memes;
          }  
          return true
        } catch (error) {
          return false
        } 
      }  
  }
})